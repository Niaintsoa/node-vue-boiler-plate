# stage  build
FROM node:14.15.4
RUN mkdir -p /usr/scr/front
WORKDIR /usr/src/front/
COPY ./client/package.json /usr/src/front/
RUN npm install
COPY ./client/ /usr/src/app/
RUN npm run build

#production environment
FROM nginx:stable-alpine
COPY --from=build /usr/src/front/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]