const state = () => ({
    sidenav: true
})

const getters = {
    sidenav: state => state.sidenav
}

const actions = {
    toggleSideNav({ commit }, value) {
        commit('toggleSideNav', value)
    }
}

const mutations = {
    toggleSideNav: (state, value) => {
        if (typeof value === 'boolean') {
            state.sidenav = value;
            return;
        }
        state.sidenav = !state.sidenav;
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}