'use strict'

const mongoose = require('mongoose')
const app = require('./app')
require('dotenv').config({ 'path': '.env' })

mongoose.connect(process.env.DB_CONNECTION, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true
})


mongoose.Promise = global.Promise

const db = mongoose.connection

db.on('error', (err) => {
    console.log(`Failed to connect with mongodb ===> ${err}`);
})

db.on('open', () => {
    console.log('Connection successfully setup');
})

const PORT = process.env.APP_PORT || 3001

const server = app.listen(PORT, () => {
    console.log(`Server running on : http://localhost:${server.address().port}`);
})
