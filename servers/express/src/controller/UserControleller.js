const User = require('../models/User')
const bcrypt = require('bcrypt-nodejs')
const jwt = require('jsonwebtoken')
const userController = {

    login: async(req, res) => {
        const { email, password, remember } = req.body

        const user = await User.findOne({ email: email })

        if (!user)
            return res.status(400).json({ message: `this account does not exist` })

        const isMatch = bcrypt.compareSync(password, user.password)

        if (!isMatch)
            return res.status(400).json({ message: 'Invalid password' })

        const payload = {
            user: {
                id: user.id
            }
        }

        // console.log(payload);

        jwt.sign(
            payload,
            'auth_admin', { expiresIn: remember === true ? "7d" : "2 days" },
            (err, token) => {
                if (err) throw err
                return res.status(200).json({
                    message: 'Login success',
                    token: token
                })

            }
        )


        console.log(req.user);


    },
    me: async(req, res) => {
        try {
            let user = await User.findById(req.user.id, '-password')
            return res.status(200).json(user)
        } catch (error) {
            return res.status(500).json(`Oops an error occured  ${error}`)
        }
    },
    getAll: async(req, res) => {
        try {
            const allUser = await User.find({ isRemoved: false }).then(response => {
                if (response)
                    return res.status(200).json(response)
            })
        } catch (error) {
            return res.status(500).json({ message: `Error occured ====> ${error}` })
        }
    },
    createUpdate: async(req, res) => {
        const data = req.body
        const user = {
            email: data.email,
            password: bcrypt.hashSync(data.password),
            username: data.username
        }


        try {
            if (!data.id) {
                const newUser = new User(user)
                await newUser.save(data).then(response => {
                    if (response) {
                        return res.status(201).json({ message: 'user created successfully', data: newUser })
                    }
                })
            }
            await User.findOneAndUpdate({ "_id": data.id }, {
                    $set: {
                        "email": user.email,
                        "username": user.username,
                        updatedAt: Date.now()
                    }
                })
                .then(response => {
                    if (response)
                        return res.status(200).json({ message: 'user data updated', data: response })
                })
        } catch (error) {
            return res.status(500).json({ message: `Error occured ======> ${error}` })
        }

    },
    deleteUser: async(req, res) => {
        const id = req.params.ID
        try {
            await User.findOneAndUpdate({ _id: id }, { $set: { isRemoved: true } }).then(response => {
                if (response)
                    return res.status(200).json({ message: 'User deleted' })
            })
        } catch (error) {
            return res.status(500).json({ message: `Error occured ====> ${error}` })
        }
    }
}

module.exports = userController